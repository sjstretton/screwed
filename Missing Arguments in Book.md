# Missing Arguments (My writing for the morning)

1. Argue that the purpose of political economy is *wealth and well-being*
2. Argue that the missing economics includes *time, space and living systems*
3. Taxation and Benefits system should be *beneficial, fair and practical*
4. There are fundamentally *three* benefits we are seeking: A safe planet, A successful country, A fair distribution of wealth and income.
5. A successful economy requires distinguishing between new capital investment and placements.