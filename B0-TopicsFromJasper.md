

# PAYING THE LANDLORD

"...So they don't have to work their entire life just to pay the frigging landlord."
This is actually IMO the biggest financial and economic problem and scandal besetting the Western world today, outside of the environmental problems.  Housing prices are on a one-way ratchet upward nearly everywhere. They almost never decline anywhere. The rentiers and banks just suck all the oxygen out of the room. People are enslaved by having to pay high rents, and their standard of living is hugely diminished because there isn't much money left for other things

----

I think you should carefully reread 14.1 and especially 14.2 and consider whether the bullet points made in 14.2 are expressed in clear and comprehensible language. The points expressed in 14.2 might require some improvement in clarity that you rather than I must supply, since they're your ideas and I would not be certain that I grok them all in the same way you do.

**Please check Line 80 in LandAndProperty to see if you can spot the content problem ("know someone who..."). This needs your attention to fix it by looking up what Shelter England said.**

*Also informally, I should tell the message that the land chapter should give. Firstly, context: we've argued that inequality of wealth is associated with unfair secondary wealth accumulation, and given both the rent on land and the appreciation both of that rent and of the overall capitalised value as examples of this unfair secondary wealth accumulation.

Secondly, for taxes to be practical they need to based on assets that can't be moved. Land is highly practical in that regard. Additionally there should be an objective means of valuation. This is less obvious, but we believe there should be no problem in determining a concrete means of valuation.

Finally, communities not only have the landlords capturing unfairly a positive externality from others, including government (there's a name for this, I think Lindhau taxes) .... but also occupation of land by normal people and businesses (esp small businesses) causes positive externalities

How to capture that effect? It's a bit more tricky, but perhaps some of the land value increase should be given back to the occupiers of land. That is owner occupiers are taxed and some of this is refunded Renters get a pure refund And absent landlords don't get any refund. (These ideas have unfortunately come to me rather later than the chapter was written; the chapter focuses on the unfair positive externality received by landlords rather the uncompensated positive externality given to the community by occupiers)

Valuation could be subtractive. Estimate the replacement construction cost of the building itself. Compare that to current market value of house-and-land Exactly We already measure bricks and mortar costs for insurance purposes. What's the problem? Note also that for the objectivity criteria we need an objective algorithm It's alright for the objective algorithm to be only an approximation to the underlying economic good. In this case in fact we can actually get a good fit for something both economically important and administratively objective (y)

Transitional justice is crucial too, and is something I am more big on than the writer of the chapter. But we do cover this to a degree. On the valuation point, valuations should be real-time indexed (approximated).  And at snapshots accurately measured Like in Denmark, but with indexation between the years. Denmark values commercial land on the odd years and residential on the even. (or the other way around; I could find a ref but I'm going back to task) (wave)

I added a couple of paragraphs explaining how to ascertain land value by subtracting from current market value the replacement cost of buildings and improvements, adjusted for depreciation  It's actually a simple and straightforward process  Create say 5 categories of house condition, ranging from very dilapidated through good as new. Take median empirically determined construction costs per square meter/ Apply a multiplier between 0.0 for a house that's a ruin to 1.0 for a new or newly renovated house. Subtract the result from current market valueInterestingly, this incentivises owners to keep buildings in tip top shape. [Exactly, but beware poor households with consequentially shit houses].  What’s the cost of German Passivhaus standard? There we have our 1.0 For new homes

Money/Banking/Finance (unless Heather has committed Corporations by then or if I've told you I've finished Environment)

The core proposal there is a bank balance sheet tax. If there's a private banking system, we want to put up the effective interest rates on mortgages and put down the effective interest rates on green lending. 

I haven't outlined that very much at all.

In addtion, we have this state-land-purchase thing going on so state captures 100% of the rent. That would be focused on areas of high appreciation potential at pre-announcement rates, just like in HK ( e.g. places that might have a new underground stop)

Heather is obsessed with behavioural nudges! Sure taxes provide a bit of that. But nudges usually relate to psychological effects more than price. And the core message I wanted to put out was this 'bads taxes are about changing the incentives for companies and corporations' Not that 'bads taxes will change consumer behaviour'. I tend to generalise massively and think of consumers as mostly habitual and non-altruistic and not even that price sensitive, whereas corps are very price sensitive: That's why they put so much sugar and carbs in breakfast cereal. Including 'healthy' cereal. It's both addictive to the consumer and cheap

----


I'm sorry for another message, but it relates to land valuation and I am going to make it here, for better or worse. The question is: should land price or land rent be the basis of tax?

There's an important distinction between measures of (property-in-toto/land   rent/purchase price - all four combinations of those) value that are reduced by an extra LVT and measures of value that are unaffected (at least by the direct mechanisms) So for example, is the 'capitalised' sale price of a house affected by the LVT directly? The answer is yes, it would be reduced. If the state captures all of the rent, the price of land would be zero. So land price is a post-tax measure.

On the other hand land rent (which could be calculated by taking measures of property rent, also available in the open market, and subtracting off the 'bricks and mortar rent') are pre-tax measures. So the specification of a 100% LVT (for example, I'm just clarifying here, not proposing that) would be different in the two cases.

In the case of the  land sale price (post-tax measure, since the tax is paid by the landlord), you have to adjust the land rent such that the price of land is zero. You can't just take some percentage of the land value, because the land exchange value is itself changed by the tax: there's a feedback. You have to instead use this target price searching method for example.

On the other hand, rents ('rental value') are paid by the tenant, and so is not directly affected by the LVT (if anything the rent is -- indirectly -- *increased*, because of the reduction in council tax paid by occupiers or the additional citizen's dividend or other tax cuts). So in this sense rental value is a 'pre-tax' measure and you can tax a percentage (e.g. 100% of the assessed land rent)

Thus, even though it's a bit harder to assess rental values compared to sale values, they might (for this reason) be a better basis for tax. That's why domestic rates (in the past in the whole UK and in Northern Ireland) and commercial rates (still used today) use 'assessed rateable value' - not quite the same thing as market value (although I'm not sure what the difference is/was exactly). 

Another thing you could do if you wanted to use capitalised values is to just add on the discounted capitalised value of the tax. But that itself needs to be estimated, adding extra complexity. Probably best to go with rents rather than sale prices.

In summary  the tax can affect the valuation. Therefore its better to choose a base that doesn't have that direct feedback (that is not affected by the tax). If you use land price as the tax base, it's gonna be affected by the tax (direct feedback). However if you use market rent (paid by the tenant) it's not affected directly by the tax

You can of course use capitalised land value still, but then the tax needs to adjust such that the land value equals zero (in the 100% case). But I reckon this find and seek approach is not really the right way of going about a tax. Market rent still needs to be transformed into land rent by being reduced by the building cost multiplied by an assumed yield

# GENERAL TOPICS

Don't frame this as a problem. It's an opportunity. Fine, we are having parallel conversations. Stop framing it negatively, it will affect your mood. Just be happy you are making AMAZING progress this week... ...and will finally deliver a text to Gordon after all this time and stress and guilt // that will be over in a couple of days

When one gets closer to something final, editing becomes an addiction

Positively enough! So I think I just need to get closer to final, and then intrinsic motivation will take over.#
I made some more changes to the intro. Now final I think!



It hardly matters at my current level of editing - and I don't yet know the difference - but I was thinking of choosing Oxford (Hart's) conventions generally rather than Chicago Manual of Style. I say so because I am just looking up stuff like where to put side notes references.1
1 https://www.amazon.co.uk/New-Harts-Rules-Oxford-Style-ebook/dp/B00MFO485W/ref=sr_1_1

BTW, check out Nicolas Berggruen, Predistribution -Berggruen Institute - Ideas for a Changing World
https://www.berggruen.org
Thanks, predistrivution seems an important topic. Here's a book Berggruen cowrote https://www.ucpress.edu/book/9780520303607/renovating-democracy

Renovating Democracy
https://www.ucpress.edu



## WEALTH FRAMEWORK

Our four fold wealth framework could be considered thus: 1) Take care of the planet 2) Take care of the national wealth (balance sheet), not just GDP, 3) Take care of communities, which should increase land values, but capture those for public purposes 4) Take care of the autonomy and minimum private wealth level of individuals.

In relation to 4, we are trying to build people up (skills) and give them more genuine economic autonomy, so they don't have to work there entire life just to pay the frigging landlord. 

 (I'd like to say 'families', but we are recommending a system based on the individual as the tax and benefit unit)

This framework was conceived after this chapter was written sadly
But it does relate to (4).
If there are any opportunities for ramming home this message throughout the book, please take them
You've got thus two matters: 1) Practical/Beneficial/Fair and 2) A framework for what is beneficial in terms of a social balance sheet (social wealth, including genuine private wealth) framework wealth in this sense meaning 'stock of genuine wealth', as opposed to 'asset value', sadly the term is overloaded
In relation to urban land this is not purely about the extractors of value (landlords), but also recognising the creators of value.

## VAT

"I'm not sure that VAT is intended to change spending choices.  That's a content point that should be corrected, yes. In relation to line 41 that's probably a good place for a graph if there isn't already one there. It's really a question of characterising that graph rather than an exact statement of fact."  removed the sentence at line 47 incorrectly saying VAT is intended to influence spending choices.  In relation to line 41, I have taken no action, and will leave it to you to find a graph. 

## WELFARE

There may well be content inconsistencies in the chapter you are doing now. For avoidance of doubt, the sort of system we are recommending is as follows:
1) A universal element to welfare, that is age dependent and also contingent on status (sickness, disability etc.)
2) Using the tax system rather than a separate means testing system to tax away benefits as income rises. So like eliminating the £11.5K personal threshold
3) For the first 4K (say) of tax paid this would be a 'contribution' record.
4) Both contributions records and work considered socially useful but unpaid (primarily child-rearing and caring) would be a contribution record. In that way, whilst we still have work disincentives in cash terms, in incentive terms you get a basic-wealth benefit for tax paid.
5) Some basic income (citizens' dividend), but really, rather small.
That is, can be paid for entirely through simplification and externality taxes