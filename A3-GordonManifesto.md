# Public Concern Management

- One of the greatest criticisms of Government is that loud pressure groups, lobbyists, and leaders of big business have too much direct access to Numbers 10 and 11 and adversely influence decision making in Government.  To combat this I propose the following:  Number 10 will create a 200-person strong --Public Concern Project Management Team (PCPMT)-- which receives submissions and calls from the public, pressure groups, lobbyists, foreigners and big business.  (All submissions will be entered on a database that is accessible directly by the Prime Minister.  The PM will have his own personal team reviewing all submissions and auditing the PCPMT to ensure that projects cannot be buried.)  Each 'customer' will receive a 'case number' and be assigned a Personal Project Manager.  
- The Project Manager will take up the issue personally and seek to resolve the issue without additional resource.  If the issue remains unresolved it will be elevated to a Senior Project Manager.  If the senior project manager is unable to resolve the issue it will go up to an Expert Committee chaired by a Cabinet Minister.  Each of these ten committees will have a team of highly skilled researchers and assistants to call upon.  Level 7 will reorganise the cabinet and government departmental responsibility around the ten books with one cabinet minister for each of the ten books.  
- Each cabinet minster will chair an Expert Committee of ten experts drawn from outside Government.  One of their tasks will be to review unresolved complaints to the Senior Project Managers of the PCPMT.  If the Expert Committee is unable to resolve the matter it will elevate to the Prime Minister, and that's when I get involved personally.  Each month all complaints will be analysed for patterns, summarized and presented in a four-hour session each week to the Prime Minister.  

The benefits of such a system are:

1. Reduce the corrupting influence by pressure groups, lobbyists, and big business, 
2. Prove to the public that we are there to help, listening to everyone, and genuinely serving the wider public through pragmatic action, 
3. Head-off social and political unrest before it gets out of control, and 
4. Ensure that corruption and incompetence in wider government and the public sector cannot metastasise and is stamped out early. 



# Reducing Bads

- **TEAR**: A system for the reduction of 'Bads' when initiated by Government -- (Possible meme:  tearing down the old system before it reduces us all to tears): 
  1) **T**ax the 'Bad'
  2) Widespread **E**ducation over a protracted period of the citizens
  3) The citizenry reaching broad **A**greement, and finally if all else fails
  4) Forced **R**estrictions and quotas on the 'bad'

-  An alternative acronym is: **EATER**:  Education, Agreement, Tax, Exercise, Experimentation, Experience & more Education, then Restrict.

# Finance

- Central Monetary Authority (CMA) responsible for the creation of all monetary credits.  Credits are uniquely numbered and traceable.  Credits are constantly analyzed for duplications.  Potentially located in Leeds not London.  Possibly move the financial capital of the UK to Leeds.
- All financial services companies nationalized without exception.  This should lower the cost of banking and insuring.
- Corporation of the City of London abolished once it has been completely encircled as discussed.
- RBS, Lloyds, Barclays (and possibly HSBC assets in the UK) and other bankrupted banking interests to be absorbed into four new State Banks.  Scientific, Industrial & Manufacturing Bank of the United Kingdom (SIMB-UK); Property, Infrastructure & Construction Bank of the United Kingdom (PICB-UK); Savings & Loan Bank of the United Kingdom (SLB-UK); and International Development Bank of the United Kingdom (IDB-UK).  Each bank will borrow money directly from the CMA and lend according to a national lending plan.  Savings will be underwritten by the state and guaranteed savings rates set.  Aimed at loans and overdrafts in excess of £50,000 so as not to compete with local Savings and Loan Mutuals.
- 650 locally owned Savings and Loan mutual organisations to be formed for each political constituency.  Each SLMO will have a local resident as Managing Director, two additional local directors (each with skin in the game -- possibly £50,000 for the MD and £25,000 each for other Directors), and a board (aufsichsrat) made up of the local MP, council representative, Union representative, and other key interested local parties.  It will follow the Bank on Dave model.  It will be allowed to do micro-loans up to £50,000 to any individual entity, with a maximum deposit of £50,000 by any local resident.  No constituency outsiders may deposit or borrow from any SLMO -- they must be completely local operations aimed at the local economy.  Burnley S&L:  5% AER on savings, and 8.9 to 14.9% interest on loans.  No bonuses of any kind to any staff.  Surpluses go to local charities.

# Property Ownership

- All assets of any kind in the UK must have a beneficial owner that can be phoned/written to, and taxed where necessary.  If a beneficial owner cannot be identified the asset will be sequestrated by the state and passed to the Citizens Dividend Trust for disposal or exploitation.  (PRC)
- Non-residents can own no property of any kind in the UK.  The resident must be the beneficial owner.  If there is any suspicion that the resident is house-sitting to enable a non-resident to speculatively own property in the UK the property will be sequestrated by the state and the resident concerned prosecuted for fraud.  (PRC)
- No property can be owned at any point in a chain of ownership by any listed secrecy/tax haven including the CCL or Delaware, for example.

# Stock Exchanges and Insurance

- All private vehicles to be automatically insured by the State Insurer for 3rd Party injury and costs - regardless of the nature and cause of the incident. Comprehensive insurance cover becomes mandatory for all drivers.  To not acquire it is criminal.  This will ensure that the 3% of drivers that fail to insure do not cause uninsured injury and costs to 3rd parties.  (I think it is Switzerland has this.)
- Once nationalized, the London Stock Exchange will be promoted as a --Traditional Stock Market-- for real investors.  Once purchased, a stock must be held for a minimum of 3 months, thereafter no more than 1% of any company can be sold in one week by a single investor, and there will be a one week delay on the sale with the final sale price being set at the end of that week.  We will sell the benefit of the LSE as being real growth in share price due to the fundamental value of the company increasing, and a revenue stream from dividends.  I believe that 'real' investors and companies will flock to London for such an opportunity.  In a way it is a niche play in a global market.  We will actively assist companies that want to relocate to the LSE.
- Smaller markets can be set up in other regional hubs e.g. Leeds, Birmingham etc. for smaller companies e.g. AIM, and a new Micro-AIM

# Basic Income

- Citizens Dividend to be paid unconditionally to all UK citizens.  New citizens must wait five years to qualify.  (Prisoners to pay 33% of their CD to the state to compensate for their keep.  The rest will be held in trust with 5% AER to cushion their release into society.  If they perish in captivity the money reverts to the CDT.)  Suggested payments:  £250 per month for 0-16 years of age; £500 for 16 to 65; and £750 thereafter. (numbers completely open to debate, as always with me)
- CD to be paid into an account at the SLB-UK.  All citizens that apply for and claim their CD must apply in person and present themselves for interview in private (if a person is considered to be vulnerable in any way, they will be provided with free state legal representation for the purposes of determining their wishes).  Each successful applicant will be issued with a banking card that doubles for a biometric Citizens Identification Card.  If a person does not want to subscribe to the ID Card system, they do not qualify for the Citizens Dividend.
- Another key policy that will negate the need for Government paying out for future pension contributions is the implementation of mandatory Social Welfare payments that we intend implementing for all registered 'tax payers'.  As in China, even if they don't pay tax, people will be required to pay a mandatory 5% to 10% of their salary into a Social Welfare fund that can be used primarily as a pension.  It will be tax free up to 10%.  That's very high.  The taxpayer can choose to receive a fixed 5% interest from the Government, or they can put up to 66% of it into riskier investments such as stocks and shares.  Then there is every incentive to save for retirement.  It will only pay out at 65 years of age.  Even putting 5% of income away for 20 or 30 years with 5% to 8% interest, and assuming we can keep inflation to 3% or below, and added to the Citizen's Dividend, should provide most people with a livable pension.  This removes much of the liability from the Government outside of the Citizen's Dividend of GBP 9,000 per year for 65+ people.

# Apprenticeships & Education

- University education to be made free or heavily subsidized for courses considered being in the 'national interest'.  This will cover a wide range of scientific, academic, and technical disciplines.  A grading system will determine the amount of subsidy from 100% free through to 100% paid.
- A massive apprenticeship-training program to be developed in direct consultation with UK businesses.  At least 250,000 apprentices entering formal technical apprenticeships each year with a minimum of 1,000,000 people under full-time hardcore technical instruction at any one time.  This will require the importation of English speaking trainers from all around the World.  The UK simply does not have enough people available to train our future generations.  A minimum of 100,000 trainers will be required, possibly as many as 250,000 employed in training and administration.
- Most prisons and detention centers to be turned into apprenticeship training hubs.  Qualifying inmates will be trained to Master Craftsman Level and, if deemed safe, will qualify to train other inmates, and eventually civilians in adjacent facilities.  Exceptional progress will be rewarded with early release if deemed appropriate.  Prison apprenticeship programs will be coordinated in close consultation with regional industry.
- Youth offenders and their families should be offered the opportunity to trade custodial sentences to technical apprenticeships on probation.

# Law and Order

- Police will have the right to ask any person to present their ID Card upon request.  Failure to do so may result in detention and further investigation.  Alternatives to presenting the card will be voluntary scan of the iris or fingers veins to determine identify.
- Police to revert to the Bobby on the Beat model.  Velvet zero tolerance policing worked spectacularly in Hartlepool and Middlesbrough under Ray Mallon.  That approach halts the progression from minor misdemeanors to major crime in a highly effective way.
- Courts to introduce fast-track/short-form sentencing for youth offenders. Under 25's should be sentenced within one week of any minor to moderate offense; any longer and the perpetrator often ceases to connect the punishment to the crime.  This fast-track process could be elective.  
- Qualifying adults sentenced to a custodial term may elect to convert their sentence to national service in the military.  Specialized military units can be formed to take these individuals.